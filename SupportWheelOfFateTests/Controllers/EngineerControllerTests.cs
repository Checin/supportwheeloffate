﻿using NUnit.Framework;
using SupportWheelOfFate.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using SupportWheelOfFate.Repositories;
using SupportWheelOfFate.Models;
using Moq;
using SupportWheelOfFate.Configuration;
using SupportWheelOfFate.ErrorHandling;

namespace SupportWheelOfFate.Controllers.Tests
{
    [TestFixture()]
    public class EngineerControllerTests
    {
        [Test()]
        public void AddEngineerOverTheLimitForRuleTest()
        {
            //Arrange
            var container = new UnityContainer();
            var engineers = Enumerable.Range(0, 11).Select(i => new Engineer());
            var fakeEngineerRepository = new Mock<IEngineerRepository>();
            var fakeSettings = new Mock<ISettingsProvider>();
            fakeSettings.SetupGet(s => s.PeriodLength).Returns(14);
            fakeSettings.SetupGet(s => s.ShiftsInADay).Returns(2);
            fakeSettings.SetupGet(s => s.MinShiftsForEngineerPerPeriod).Returns(2);
            fakeSettings.SetupGet(s => s.CurrentDate).Returns(new DateTime(2018, 1, 3));
            fakeEngineerRepository.Setup(repository => repository.GetAll()).Returns(() => engineers);
            container.RegisterInstance(fakeEngineerRepository.Object);
            container.RegisterInstance(fakeSettings.Object);
            container.RegisterInstance(new Mock<IEntryRepository>().Object);
            var engineerController = container.Resolve<EngineerController>();

            //Act
            TestDelegate result = () => engineerController.AddOrUpdate(new Engineer());

            //Assert
            Assert.Throws<BussinessException>(result);
        }

        [Test()]
        public void AddEngineerUnderTheLimitForRuleTest()
        {
            //Arrange
            var container = new UnityContainer();
            var engineers = Enumerable.Range(0, 9).Select(i => new Engineer());
            var fakeEngineerRepository = new Mock<IEngineerRepository>();
            var fakeSettings = new Mock<ISettingsProvider>();
            fakeSettings.SetupGet(s => s.PeriodLength).Returns(14);
            fakeSettings.SetupGet(s => s.ShiftsInADay).Returns(2);
            fakeSettings.SetupGet(s => s.MinShiftsForEngineerPerPeriod).Returns(2);
            fakeSettings.SetupGet(s => s.CurrentDate).Returns(new DateTime(2018, 1, 3));
            fakeEngineerRepository.Setup(repository => repository.GetAll()).Returns(() => engineers);
            container.RegisterInstance(fakeEngineerRepository.Object);
            container.RegisterInstance(fakeSettings.Object);
            container.RegisterInstance(new Mock<IEntryRepository>().Object);
            var engineerController = container.Resolve<EngineerController>();

            //Act
            TestDelegate result = () => engineerController.AddOrUpdate(new Engineer());

            //Assert
            Assert.DoesNotThrow(result);
        }
    }
}