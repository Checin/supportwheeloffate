﻿using NUnit.Framework;
using SupportWheelOfFate.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Moq;
using SupportWheelOfFate.Repositories;
using SupportWheelOfFate.Configuration;
using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Generators.Tests
{
    [TestFixture()]
    public class WheelEntriesGeneratorTests
    {
        [Test()]
        public void DeleteEntriesForDateRangeTest()
        {
            //Arrange
            UnityContainer container = new UnityContainer();
            var dateRange = new DateRange(new DateTime(2018, 1, 5), new DateTime(2018, 1, 7));
            var entries = new List<Entry>()
            {
                new Entry { Date = new DateTime(2018, 1, 4), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 6), EngineerId = 3 },
                new Entry { Date = new DateTime(2018, 1, 7), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 7), EngineerId = 3 }
            };
            var fakeEntryRepository = new Mock<IEntryRepository>();
            fakeEntryRepository.Setup(repository => repository.GetAll()).Returns(() => entries);
            container.RegisterInstance(fakeEntryRepository.Object);
            container.RegisterInstance(new Mock<IEngineerRepository>().Object);
            container.RegisterInstance(new Mock<ISettingsProvider>().Object);
            var entriesGenerator = container.Resolve<EntriesGenerator>();

            //Act
            entriesGenerator.DeleteEntriesForDateRange(dateRange);

            //Assert
            fakeEntryRepository.Verify(m => m.DeleteRange(new List<Entry>()
            {
                entries[1],
                entries[2]
            }));
        }

        [Test()]
        public void GenerateAndSaveEntriesForDateRangeTest()
        {
            //Arrange
            List<Entry> result = new List<Entry>();
            UnityContainer container = new UnityContainer();
            var dateRange = new DateRange(new DateTime(2018, 1, 3), new DateTime(2018, 1, 5));
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" },
                new Engineer() { EngineerId = 2, Name = "Engineer2" },
                new Engineer() { EngineerId = 3, Name = "Engineer3" },
                new Engineer() { EngineerId = 4, Name = "Engineer4" }
            };
            var fakeEntryRepository = new Mock<IEntryRepository>();

            fakeEntryRepository.Setup(repository => 
                repository.GetByDate(It.IsInRange(new DateTime(2018, 1, 3), new DateTime(2018, 1, 4), Range.Inclusive)))
                .Returns((Func<DateTime, IEnumerable<Entry>>)((date) => result.Where(e => e.Date == date)));

            var fakeEngineerRepository = new Mock<IEngineerRepository>();
            fakeEngineerRepository.Setup(repository => repository.GetAll()).Returns(() => engineers);
            var fakeSettingsProvider = new Mock<ISettingsProvider>();
            fakeSettingsProvider.SetupGet(m => m.CanHaveShiftsOnConsecutiveDays).Returns(false);
            fakeSettingsProvider.SetupGet(m => m.MaxShiftsForEngineerPerDay).Returns(1);
            fakeSettingsProvider.SetupGet(m => m.MinShiftsForEngineerPerPeriod).Returns(1);
            fakeSettingsProvider.SetupGet(m => m.PeriodLength).Returns(2);
            fakeSettingsProvider.SetupGet(m => m.ShiftsInADay).Returns(2);
            container.RegisterInstance(fakeEntryRepository.Object);
            container.RegisterInstance(fakeEngineerRepository.Object);
            container.RegisterInstance(fakeSettingsProvider.Object);
            var entriesGenerator = container.Resolve<EntriesGenerator>();

            //Act
            result = entriesGenerator.GenerateAndSaveEntriesForDateRange(dateRange);

            //Assert
            Assert.AreEqual(4, result.Count(), "Result count is wrong");
            Assert.AreEqual(4, result.Count(e => e.Date >= new DateTime(2018, 1, 3) && e.Date < new DateTime(2018, 1, 5)), 
                    "Entries count in period is wrong");
            Assert.AreEqual(1, result.Count(e => e.EngineerId == 1), "Entries count for engineer1 is wrong");
            Assert.AreEqual(1, result.Count(e => e.EngineerId == 2), "Entries count for engineer2 is wrong");
            Assert.AreEqual(1, result.Count(e => e.EngineerId == 3), "Entries count for engineer3 is wrong");
            Assert.AreEqual(1, result.Count(e => e.EngineerId == 4), "Entries count for engineer4 is wrong");
        }
    }
}