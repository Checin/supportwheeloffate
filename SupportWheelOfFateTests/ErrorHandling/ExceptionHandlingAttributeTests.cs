﻿using NUnit.Framework;
using SupportWheelOfFate.ErrorHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Filters;
using System.Web.Http;
using System.Net.Http;
using Unity;
using Moq;

namespace SupportWheelOfFate.ErrorHandling.Tests
{
    [TestFixture()]
    public class ExceptionHandlingAttributeTests
    {
        [Test()]
        public void OnExceptionTest()
        {
            //Arrange
            UnityContainer container = new UnityContainer();
            var exceptionHandling = container.Resolve<ExceptionHandlingAttribute>();
            HttpActionExecutedContext context = new HttpActionExecutedContext()
            {
                Exception = new Exception("Unexpected test exception")
            };

            //Act
            TestDelegate result = () => exceptionHandling.OnException(context);

            //Assert
            Assert.Throws<HttpResponseException>(result, "Unexpected error occured");
        }

        [Test()]
        public void OnBusinessExceptionTest()
        {
            //Arrange
            UnityContainer container = new UnityContainer();
            var exceptionHandling = container.Resolve<ExceptionHandlingAttribute>();
            HttpActionExecutedContext context = new HttpActionExecutedContext()
            {
                Exception = new BussinessException("test exception message", System.Net.HttpStatusCode.NotFound)
            };

            //Act
            TestDelegate result = () => exceptionHandling.OnException(context);

            //Assert
            Assert.Throws<HttpResponseException>(result, "test exception message");
        }
    }
}