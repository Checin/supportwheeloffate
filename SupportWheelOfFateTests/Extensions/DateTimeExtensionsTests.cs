﻿using NUnit.Framework;
using SupportWheelOfFate.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportWheelOfFate.Extensions.Tests
{
    [TestFixture()]
    public class DateTimeExtensionsTests
    {
        [Test()]
        public void IsWeekdayFalseForWeekendTest()
        {
            //Arrange
            DateTime saturday = new DateTime(2018, 1, 6);
            DateTime sunday = new DateTime(2018, 1, 7);
            bool saturayResult, sundayResult;

            //Act
            saturayResult = saturday.IsWeekday();
            sundayResult = sunday.IsWeekday();

            //Assert
            Assert.IsFalse(saturayResult);
            Assert.IsFalse(sundayResult);
        }

        [Test()]
        public void IsWeekdayTrueForWorkingDaysTest()
        {
            //Arrange
            DateTime friday = new DateTime(2018, 1, 5);
            DateTime wednesday = new DateTime(2018, 1, 3);
            bool fridayResult, wednesdayResult;

            //Act
            fridayResult = friday.IsWeekday();
            wednesdayResult = wednesday.IsWeekday();

            //Assert
            Assert.IsTrue(fridayResult);
            Assert.IsTrue(wednesdayResult);
        }

        [Test()]
        public void GetPreviousWeekdayAvoidingWeekendTest()
        {
            //Arrange
            DateTime monday = new DateTime(2018, 1, 8);
            DateTime sunday = new DateTime(2018, 1, 7);
            DateTime mondayResult, sundayResult;

            //Act
            mondayResult = monday.GetPreviousWeekday();
            sundayResult = sunday.GetPreviousWeekday();

            //Assert
            Assert.AreEqual(mondayResult, new DateTime(2018, 1, 5));
            Assert.AreEqual(sundayResult, new DateTime(2018, 1, 5));
        }

        [Test()]
        public void GetPreviousWeekdayTest()
        {
            //Arrange
            DateTime friday = new DateTime(2018, 1, 5);
            DateTime wednesday = new DateTime(2018, 1, 3);
            DateTime fridayResult, wednesdayResult;

            //Act
            fridayResult = friday.GetPreviousWeekday();
            wednesdayResult = wednesday.GetPreviousWeekday();

            //Assert
            Assert.AreEqual(fridayResult, new DateTime(2018, 1, 4));
            Assert.AreEqual(wednesdayResult, new DateTime(2018, 1, 2));
        }
    }
}