﻿using NUnit.Framework;
using SupportWheelOfFate.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SupportWheelOfFate.Extensions.Tests
{
    [TestFixture()]
    public class EngineerExtensionsTests
    {
        [Test()]
        public void ExcludeEngineerIdsTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" },
                new Engineer() { EngineerId = 2, Name = "Engineer2" },
                new Engineer() { EngineerId = 3, Name = "Engineer3" },
                new Engineer() { EngineerId = 4, Name = "Engineer4" }
            };
            IEnumerable<int> toExclude = new List<int>() { 2, 3 };

            //Act
            var result = engineers.ExcludeEngineerIds(toExclude).ToList();

            //Assert
            Assert.AreEqual(new List<Engineer>()
            {
                engineers.First(),
                engineers.Last()
            }, result);
        }

        [Test()]
        public void ExcludeEngineerIdsWithEmptyExcludeListTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" },
                new Engineer() { EngineerId = 2, Name = "Engineer2" },
                new Engineer() { EngineerId = 3, Name = "Engineer3" },
                new Engineer() { EngineerId = 4, Name = "Engineer4" }
            };
            IEnumerable<int> toExclude = new List<int>() { };

            //Act
            var result = engineers.ExcludeEngineerIds(toExclude).ToList();

            //Assert
            Assert.AreEqual(engineers, result);
        }

        [Test()]
        public void GetRandomEngineerOneElementTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" }
            };

            //Act
            var result = engineers.GetRandom();

            //Assert
            Assert.AreEqual(engineers.First(), result);
        }

        [Test()]
        public void GetRandomEngineerNoElementsTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>() { };

            //Act
            TestDelegate result = () => engineers.GetRandom();

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(result);
        }

        [Test()]
        public void EngineersWithoutMinShiftsInPeriodTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" },
                new Engineer() { EngineerId = 2, Name = "Engineer2" },
                new Engineer() { EngineerId = 3, Name = "Engineer3" },
                new Engineer() { EngineerId = 4, Name = "Engineer4" }
            };
            IEnumerable<Entry> entries = new List<Entry>()
            {
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 6), EngineerId = 3 },
                new Entry { Date = new DateTime(2018, 1, 7), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 7), EngineerId = 3 }
            };

            //Act
            var result = engineers.WithLeastShiftsInPeriod(entries).ToList();

            //Assert
            Assert.AreEqual(new List<Engineer>()
            {
                engineers.Last()
            }, result);
        }

        [Test()]
        public void EngineersWithLeastShiftsInPeriodSameShiftCountTest()
        {
            //Arrange
            IEnumerable<Engineer> engineers = new List<Engineer>()
            {
                new Engineer() { EngineerId = 1, Name = "Engineer1" },
                new Engineer() { EngineerId = 2, Name = "Engineer2" },
                new Engineer() { EngineerId = 3, Name = "Engineer3" },
                new Engineer() { EngineerId = 4, Name = "Engineer4" }
            };
            IEnumerable<Entry> entries = new List<Entry>()
            {
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 6), EngineerId = 3 },
                new Entry { Date = new DateTime(2018, 1, 6), EngineerId = 4 },
            };

            //Act
            var result = engineers.WithLeastShiftsInPeriod(entries).ToList();

            //Assert
            Assert.AreEqual(engineers, result);
        }
    }
}