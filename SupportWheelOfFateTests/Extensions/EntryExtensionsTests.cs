﻿using NUnit.Framework;
using SupportWheelOfFate.Models;
using SupportWheelOfFate.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportWheelOfFate.Extensions.Tests
{
    [TestFixture()]
    public class EntryExtensionsTests
    {
        [Test()]
        public void InDateRangeTest()
        {
            //Arrange
            List<Entry> entries = new List<Entry>()
            {
                new Entry { Date = new DateTime(2018, 1, 4), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 2 },
                new Entry { Date = new DateTime(2018, 1, 7), EngineerId = 3 },
                new Entry { Date = new DateTime(2018, 1, 8), EngineerId = 4 },
            };
            DateRange range = new DateRange(new DateTime(2018, 1, 5), new DateTime(2018, 1, 8));

            //Act
            var result = entries.InDateRange(range);

            //Assert
            Assert.AreEqual(new List<Entry>()
            {
                entries[1],
                entries[2]
            }, result);
        }

        [Test()]
        public void EngineerIdsWithSpecifiedShiftsCountTest()
        {
            //Arrange
            IEnumerable<Entry> entries = new List<Entry>()
            {
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 1 },
                new Entry { Date = new DateTime(2018, 1, 5), EngineerId = 3 },
            };
            int maxShifts = 2;

            //Act
            var result = entries.EngineerIdsWithSpecifiedShiftsCount(maxShifts).ToList();

            //Assert
            Assert.AreEqual(new List<int>() { 1 }, result);
        }
    }
}