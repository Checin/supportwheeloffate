﻿using NUnit.Framework;
using SupportWheelOfFate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Helpers.Tests
{
    [TestFixture()]
    public class WheelHelperTests
    {
        [Test()]
        public void GetWeekdaysTest()
        {
            //Arrange
            var range = new DateRange(new DateTime(2018, 1, 4), new DateTime(2018, 1, 7));

            //Act
            var result = WheelHelper.GetWeekdays(range);

            //Assert
            Assert.AreEqual(new List<DateTime>()
            {
                new DateTime(2018, 1, 4),
                new DateTime(2018, 1, 5)
            }, result);
        }

        [Test()]
        public void GetPeriodDateRangeTest()
        {
            //Act
            var result0 = WheelHelper.GetPeriodDateRange(0, new DateTime(2018, 1, 3), 7, new DateTime(2018, 1, 4));
            var result1 = WheelHelper.GetPeriodDateRange(1, new DateTime(2018, 1, 3), 14, new DateTime(2018, 1, 4));

            //Assert
            Assert.AreEqual(new DateRange(new DateTime(2018, 1, 3), new DateTime(2018, 1, 10)), result0);
            Assert.AreEqual(new DateRange(new DateTime(2018, 1, 17), new DateTime(2018, 1, 31)), result1);
        }
    }
}