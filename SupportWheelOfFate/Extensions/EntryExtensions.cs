﻿using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Extensions
{
    public static class EntryExtensions
    {
        public static IEnumerable<Entry> InDateRange(this IEnumerable<Entry> entries, DateRange range)
        {
            return entries.Where(e => e.Date >= range.StartDate && e.Date < range.EndDate);
        }

        public static IEnumerable<int> EngineerIdsWithSpecifiedShiftsCount(this IEnumerable<Entry> entries, int maxShifts)
        {
            return entries.GroupBy(e => e.EngineerId).Where(g => g.Count() >= maxShifts).Select(g => g.Key);
        }
    }
}