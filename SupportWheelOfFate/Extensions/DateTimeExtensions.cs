﻿using System;

namespace SupportWheelOfFate.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsWeekday(this DateTime date)
        {
            return date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static DateTime GetPreviousWeekday(this DateTime date)
        {
            do
            {
                date = date.AddDays(-1);
            }
            while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday);

            return date;
        }
    }
}