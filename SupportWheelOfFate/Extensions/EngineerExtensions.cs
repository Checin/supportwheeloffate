﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Extensions
{
    public static class EngineerExtensions
    {
        public static IEnumerable<Engineer> WithLeastShiftsInPeriod(this IEnumerable<Engineer> engineers,
                                                                                IEnumerable<Entry> entriesInPeriod)
                                                                                
        {
            return engineers.GroupBy(e => entriesInPeriod.Count(entry => entry.EngineerId == e.EngineerId))
                                            .OrderBy(g => g.Key).First().Select(elem => elem);
        }

        public static Engineer GetRandom(this IEnumerable<Engineer> engineerCollection)
        {
            return engineerCollection.ElementAt(new Random().Next(engineerCollection.Count()));
        }

        public static IEnumerable<Engineer> ExcludeEngineerIds(this IEnumerable<Engineer> engineers, IEnumerable<int> excluded)
        {
            return engineers.Where(e => !excluded.Contains(e.EngineerId));
        }
    }
}