﻿using System.Web.Http.Filters;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Unity.Attributes;

namespace SupportWheelOfFate.ErrorHandling
{
    public class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            StringContent responseContent;
            var statusCode = HttpStatusCode.InternalServerError;
            if (actionExecutedContext.Exception is BussinessException)
            {
                responseContent = new StringContent(actionExecutedContext.Exception.Message);
                statusCode = ((BussinessException)actionExecutedContext.Exception).StatusCode;
            }
            else
            {
                responseContent = new StringContent("Unexpected error occured");
            }

            throw new HttpResponseException(new HttpResponseMessage(statusCode)
            {
                Content = responseContent,
                ReasonPhrase = "Exception"
            });
        }
    }
}