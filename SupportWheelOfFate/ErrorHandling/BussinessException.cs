﻿using System;
using System.Net;

namespace SupportWheelOfFate.ErrorHandling
{
    public class BussinessException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public BussinessException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}