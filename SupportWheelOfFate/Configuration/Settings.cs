﻿using System;
using System.Configuration;

namespace SupportWheelOfFate.Configuration
{
    public class Settings : ISettingsProvider
    {
        public int PeriodLength => Convert.ToInt32(ConfigurationManager.AppSettings["PeriodLength"]);

        public int MinShiftsForEngineerPerPeriod => Convert.ToInt32(ConfigurationManager.AppSettings["MinShiftsForEngineerPerPeriod"]);

        public int ShiftsInADay => Convert.ToInt32(ConfigurationManager.AppSettings["ShiftsInADay"]);

        public int MaxShiftsForEngineerPerDay => Convert.ToInt32(ConfigurationManager.AppSettings["MaxShiftsForEngineerPerDay"]);

        public bool CanHaveShiftsOnConsecutiveDays => Convert.ToBoolean(ConfigurationManager.AppSettings["CanHaveShiftsOnConsecutiveDays"]);

        public DateTime CurrentDate => DateTime.Now;
    }
}