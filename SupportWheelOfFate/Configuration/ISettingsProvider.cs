﻿using System;

namespace SupportWheelOfFate.Configuration
{
    public interface ISettingsProvider
    {
        int PeriodLength { get; }
        int MinShiftsForEngineerPerPeriod { get; }
        int ShiftsInADay { get; }
        int MaxShiftsForEngineerPerDay { get; }
        DateTime CurrentDate { get; }
        bool CanHaveShiftsOnConsecutiveDays { get; }
    }
}
