﻿using System;
using System.Collections.Generic;
using SupportWheelOfFate.Models;
using SupportWheelOfFate.Extensions;

namespace SupportWheelOfFate.Helpers
{
    public static class WheelHelper
    {
        public static List<DateTime> GetWeekdays(DateRange dateRange)
        {
            var result = new List<DateTime>();
            var startDate = dateRange.StartDate;
            while (startDate < dateRange.EndDate)
            {
                if (startDate.IsWeekday())
                {
                    result.Add(startDate);
                }
                startDate = startDate.AddDays(1);
            }
            return result;
        }

        public static DateRange GetPeriodDateRange(int period, DateTime firstPeriodStart, int periodLength, DateTime currentDate)
        {
            DateTime startDate = GetPeriodStartDate(period, firstPeriodStart, periodLength, currentDate);
            return new DateRange(startDate, GetPeriodEndDate(startDate, periodLength));
        }

        private static int GetAbsolutePeriodNumber(int period, DateTime startDate, int periodLength)
        {
            return (int)((DateTime.Now - startDate).TotalDays / periodLength) + period;
        }

        private static DateTime GetPeriodStartDate(int period, DateTime firstPeriodStart, int periodLength, DateTime currentDate)
        {
            var result = firstPeriodStart.AddDays(periodLength * GetAbsolutePeriodNumber(period, firstPeriodStart, periodLength));
            if (result <= currentDate)
            {
                return result;
            }
            while (result > currentDate)
            {
                result = result.AddDays(-periodLength);
            }
            result = result.AddDays(periodLength * period);
            return result;
        }

        private static DateTime GetPeriodEndDate(DateTime startDate, int periodLength)
        {
            return startDate.AddDays(periodLength);
        }
    }
}