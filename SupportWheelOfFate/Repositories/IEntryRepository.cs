﻿using SupportWheelOfFate.Models;
using System;
using System.Collections.Generic;

namespace SupportWheelOfFate.Repositories
{
    public interface IEntryRepository : IWheelRepository<Entry>
    {
        IEnumerable<Entry> GetByDate(DateTime date);
        void DeleteRange(IEnumerable<Entry> entries);
        void DeleteEntriesInDateRange(DateRange dateRange);
    }
}
