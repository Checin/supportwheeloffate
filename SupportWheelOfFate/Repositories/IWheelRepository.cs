﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportWheelOfFate.Repositories
{
    public interface IWheelRepository<T>
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        void DeleteById(int id);
        void Delete(T toDelete);
        void Create(T toAdd);
        int SaveChanges();
    }
}