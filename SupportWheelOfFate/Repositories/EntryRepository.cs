﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.Models;
using SupportWheelOfFate.Extensions;
using System.Data.Entity;
using SupportWheelOfFate.ErrorHandling;

namespace SupportWheelOfFate.Repositories
{
    public class EntryRepository : IEntryRepository, IDisposable
    {
        private readonly WheelContext context = new WheelContext();

        public void Create(Entry entry)
        {
            context.Entries.Add(entry);
        }

        public void DeleteById(int id)
        {
            if (GetById(id) == null)
            {
                throw new BussinessException("Couldn't find entry with id " + id, System.Net.HttpStatusCode.BadRequest);
            }
            context.Entries.Remove(GetById(id));
        }

        public void DeleteRange(IEnumerable<Entry> entries)
        {
            context.Entries.RemoveRange(entries);
        }

        public void Delete(Entry entry)
        {
            context.Entries.Remove(entry);
        }

        public void DeleteEntriesInDateRange(DateRange dateRange)
        {
            var entries = GetAll().InDateRange(dateRange);
            if (entries.Count() == 0)
            {
                throw new BussinessException("No entries found", System.Net.HttpStatusCode.BadRequest);
            }
            DeleteRange(entries);
        }

        public IEnumerable<Entry> GetAll()
        {
            return context.Entries;
        }

        public Entry GetById(int id)
        {
            return context.Entries.FirstOrDefault(e => e.EntryId == id);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public IEnumerable<Entry> GetByDate(DateTime date)
        {
            return context.Entries.Where(e => DbFunctions.TruncateTime(e.Date) == date.Date);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                context?.Dispose();
            }
        }
    }
}