﻿using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Repositories
{
    public interface IEngineerRepository : IWheelRepository<Engineer>
    {
        void UpdateEngineer(Engineer engineer);
    }
}
