﻿using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.ErrorHandling;
using System.Net;
using SupportWheelOfFate.Models;
using System;

namespace SupportWheelOfFate.Repositories
{
    public class EngineerRepository : IEngineerRepository, IDisposable
    {
        private readonly WheelContext context = new WheelContext();

        public void Create(Engineer engineer)
        {
            context.Engineers.Add(engineer);
        }

        public void UpdateEngineer(Engineer engineer)
        {
            var toEdit = context.Engineers.FirstOrDefault(e => e.EngineerId == engineer.EngineerId);
            if (toEdit == null)
            {
                throw new BussinessException("Couldn't find engineer with id " + engineer.EngineerId, HttpStatusCode.BadRequest);
            }
            toEdit.Name = engineer.Name;
        }

        public void DeleteById(int id)
        {
            var engineer = GetById(id);
            if (engineer == null)
            {
                throw new BussinessException("Couldn't find engineer with id " + id, HttpStatusCode.BadRequest);
            }
            Delete(engineer);
        }

        public void Delete(Engineer engineer)
        {
            context.Engineers.Remove(engineer);
        }

        public IEnumerable<Engineer> GetAll()
        {
            return context.Engineers;
        }

        public Engineer GetById(int id)
        {
            return context.Engineers.FirstOrDefault(e => e.EngineerId == id);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                context?.Dispose();
            }
        }
    }
}