﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.Models;
using SupportWheelOfFate.Repositories;
using SupportWheelOfFate.Configuration;
using SupportWheelOfFate.Extensions;
using SupportWheelOfFate.Helpers;

namespace SupportWheelOfFate.Generators
{
    public class EntriesGenerator : IEntriesGenerator
    {
        readonly IEntryRepository _entryRepository;
        readonly IEngineerRepository _engineerRepository;
        readonly ISettingsProvider _settings;

        public EntriesGenerator(IEntryRepository entryRepository, 
                                    IEngineerRepository engineerRepository, 
                                    ISettingsProvider settings)
        {
            _entryRepository = entryRepository;
            _engineerRepository = engineerRepository;
            _settings = settings;
        }

        public void DeleteEntriesForDateRange(DateRange dateRange)
        {
            _entryRepository.DeleteRange(_entryRepository.GetAll().InDateRange(dateRange));
            _entryRepository.SaveChanges();
        }

        public List<Entry> GenerateAndSaveEntriesForDateRange(DateRange dateRange)
        {
            var generated = new List<Entry>();

            var engineers = _engineerRepository.GetAll().ToList();
            if (!engineers.Any())
            {
                throw new ErrorHandling.BussinessException("There are no engineers in database", System.Net.HttpStatusCode.NotFound);
            }

            DeleteEntriesForDateRange(dateRange);
            _entryRepository.SaveChanges();

            foreach (var date in WheelHelper.GetWeekdays(dateRange))
            {
                while (generated.Count(e => e.Date == date) < _settings.ShiftsInADay)
                {
                    var newEntry = GenerateEntryForDate(date, engineers, generated);
                    _entryRepository.Create(newEntry);
                    generated.Add(newEntry);
                }
            }
            _entryRepository.SaveChanges();
            return generated;
        }

        private Entry GenerateEntryForDate(DateTime date, List<Engineer> engineers, List<Entry> entriesInPeriod)
        {
            IEnumerable<int> excludedEngineerIds = entriesInPeriod.Where(e => e.Date == date)
                                        .EngineerIdsWithSpecifiedShiftsCount(_settings.MaxShiftsForEngineerPerDay).ToList();

            if (!_settings.CanHaveShiftsOnConsecutiveDays)
            {
                IEnumerable<int> fromPreviousWeekday = GetEngineerIdsFromPreviousWeekday(date, entriesInPeriod).ToList();
                if (!fromPreviousWeekday.Any())
                {
                    fromPreviousWeekday = GetEngineerIdsFromPreviousWeekday(date);
                }
                excludedEngineerIds = excludedEngineerIds.Union(fromPreviousWeekday);
            }

            var engineersToDraw = engineers.ExcludeEngineerIds(excludedEngineerIds)
                                           .WithLeastShiftsInPeriod(entriesInPeriod);

            return new Entry { Date = date, EngineerId = engineersToDraw.GetRandom().EngineerId };
        }

        private IEnumerable<int> GetEngineerIdsFromPreviousWeekday(DateTime date, IEnumerable<Entry> entries)
        {
            return entries.Where(e => e.Date == date.GetPreviousWeekday()).Select(e => e.EngineerId);
        }

        private IEnumerable<int> GetEngineerIdsFromPreviousWeekday(DateTime date)
        {
            return _entryRepository.GetByDate(date.GetPreviousWeekday()).Select(e => e.EngineerId);
        }
    }
}