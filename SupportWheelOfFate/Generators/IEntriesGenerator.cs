﻿using System.Collections.Generic;
using SupportWheelOfFate.Models;

namespace SupportWheelOfFate.Generators
{
    public interface IEntriesGenerator
    {
        List<Entry> GenerateAndSaveEntriesForDateRange(DateRange range);
    }
}
