﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelOfFate.Extensions;
using SupportWheelOfFate.Repositories;
using SupportWheelOfFate.Models;
using SupportWheelOfFate.Configuration;
using SupportWheelOfFate.Helpers;
using SupportWheelOfFate.Generators;
using SupportWheelOfFate.ErrorHandling;

namespace SupportWheelOfFate.Managers
{
    public class WheelManager
    {
        private readonly IEntryRepository _entryRepository;
        private readonly IEntriesGenerator _entriesGenerator;
        private readonly ISettingsProvider _settings;

        public WheelManager(IEntryRepository entryRepository, 
                            ISettingsProvider settings,
                            IEntriesGenerator entriesGenerator)
        {
            _entryRepository = entryRepository;
            _settings = settings;
            _entriesGenerator = entriesGenerator;
        }

        public IEnumerable<Entry> GetEntriesForPeriod(int period)
        {
            var firstPeriodStartDate = GetFirstPeriodStartDate();
            var dateRange = WheelHelper.GetPeriodDateRange(period, firstPeriodStartDate, _settings.PeriodLength, _settings.CurrentDate);
            if (period < 0 && dateRange.StartDate < firstPeriodStartDate)
            {
                throw new BussinessException("Entries for period " + period + " not found", System.Net.HttpStatusCode.NotFound);
            }
            return GenerateOrGetSavedEntries(dateRange);
        }

        public void DeleteEntriesForPeriod(int period)
        {
            DateRange dateRange = 
                WheelHelper.GetPeriodDateRange(period, GetFirstPeriodStartDate(), _settings.PeriodLength, _settings.CurrentDate);
            _entryRepository.DeleteEntriesInDateRange(dateRange);
            _entryRepository.SaveChanges();
        }

        public void DeleteAllEntries()
        {
            var entries = _entryRepository.GetAll();
            if (entries.Count() == 0)
            {
                throw new ErrorHandling.BussinessException("No entries found", System.Net.HttpStatusCode.BadRequest);
            }
            _entryRepository.DeleteRange(entries);
            _entryRepository.SaveChanges();
        }

        private DateTime GetFirstPeriodStartDate()
        {
            var entries = _entryRepository.GetAll();
            DateTime firstPeriodStart = DateTime.Now.Date;
            if (entries.Any())
            {
                firstPeriodStart = entries.Min(e => e.Date);
            }
            return firstPeriodStart;
        }

        private IEnumerable<Entry> GenerateOrGetSavedEntries(DateRange dateRange)
        {
            if (!CheckEntriesCorrectness(dateRange))
            {
                _entriesGenerator.GenerateAndSaveEntriesForDateRange(dateRange);
            }
            return GetSavedEntriesInDateRange(dateRange);
        }

        private IEnumerable<Entry> GetSavedEntriesInDateRange(DateRange dateRange)
        {
            return _entryRepository.GetAll().InDateRange(dateRange);
        }

        private bool CheckEntriesCorrectness(DateRange range)
        {
            if (!CheckShiftsCount(range) 
                || !CheckShiftsCountPerDay(range)
                || !CheckMinShiftsForEngineerInPeriod(range))
            {
                return false;
            }

            if (_settings.CanHaveShiftsOnConsecutiveDays)
            {
                return true;
            }

            var weekdays = WheelHelper.GetWeekdays(range);
            weekdays.Add(weekdays.First().GetPreviousWeekday());

            return CheckConsecutiveDaysShifts(weekdays);
        }

        private bool CheckMinShiftsForEngineerInPeriod(DateRange range)
        {
            return _entryRepository.GetAll().InDateRange(range)
                .GroupBy(e => e.EngineerId).All(g => g.Count() == _settings.MinShiftsForEngineerPerPeriod);
        }

        private bool CheckShiftsCountPerDay(DateRange range)
        {
            foreach (var day in _entryRepository.GetAll().InDateRange(range).GroupBy(e => e.Date))
            {
                if (day.Count() != _settings.ShiftsInADay)
                //overall shifts in day
                {
                    return false;
                }
                if (day.Select(e => e.EngineerId).GroupBy(e => e).Any(g => g.Count() > _settings.MaxShiftsForEngineerPerDay))
                //shifts per engineer in a day
                {
                    return false;
                }
            }
            return true;
        }

        private bool CheckShiftsCount(DateRange range)
        {
            return _entryRepository.GetAll().InDateRange(range).Count() == _settings.ShiftsInADay * WheelHelper.GetWeekdays(range).Count();
        }

        private bool CheckConsecutiveDaysShifts(List<DateTime> weekdays)
        {
            for (int i = 1; i < weekdays.Count(); i++)
            {
                var previousDayEntries = _entryRepository.GetByDate(weekdays[i - 1]).ToList();
                var currentDayEntries = _entryRepository.GetByDate(weekdays[i]).ToList();
                if (previousDayEntries.FirstOrDefault(e => currentDayEntries.Any(e1 => e1.EngineerId == e.EngineerId)) != null)
                {
                    return false;
                }
            }
            return true;
        }
    }
}