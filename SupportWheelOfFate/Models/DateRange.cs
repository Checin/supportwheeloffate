﻿using System;

namespace SupportWheelOfFate.Models
{
    public struct DateRange
    {
        /// <summary>
        /// Initializes instance of DateRange
        /// </summary>
        /// <param name="startDate">Inclusive start date</param>
        /// <param name="endDate">Exclusive end date</param>
        public DateRange(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate.Date;
            _endDate = endDate.Date;
        }

        private DateTime _startDate;
        public DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value.Date;
            }
        }

        private DateTime _endDate;
        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value.Date;
            }
        }
    }
}