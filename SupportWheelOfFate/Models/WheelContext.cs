﻿using System.Data.Entity;

namespace SupportWheelOfFate.Models
{
    public class WheelContext : DbContext
    {
        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Entry> Entries { get; set; }
    }
}