﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SupportWheelOfFate.Models
{
    /// <summary>
    /// Represents one engineer’s shift
    /// </summary>
    public class Entry
    {
        /// <summary>
        /// Unique id of entry
        /// </summary>
        [Key]
        public int EntryId { get; set; }

        /// <summary>
        /// Entry's date
        /// </summary>
        [Required]
        public DateTime Date { get; set; }

        /// <summary>
        /// Id of an engineer related to this entry
        /// </summary>
        public int EngineerId { get; set; }

        /// <summary>
        /// Related engineer’s instance
        /// </summary>
        public virtual Engineer Engineer { get; set; }
    }
}