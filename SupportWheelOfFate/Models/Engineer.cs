﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SupportWheelOfFate.Models
{
    /// <summary>
    /// Represents engineer
    /// </summary>
    public class Engineer
    {
        /// <summary>
        /// Unique id of an engineer
        /// </summary>
        [Key]
        public int EngineerId { get; set; }

        /// <summary>
        /// Engineer's name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Collection of entries related to this engineer
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Entry> Entries { get; set; }
    }
}