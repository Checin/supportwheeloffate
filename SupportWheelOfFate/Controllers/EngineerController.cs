﻿using System.Collections.Generic;
using System.Web.Http;
using SupportWheelOfFate.Models;
using System.Web.Http.Cors;
using System.Net;
using System;
using System.Linq;
using SupportWheelOfFate.Repositories;
using SupportWheelOfFate.ErrorHandling;
using SupportWheelOfFate.Configuration;

namespace SupportWheelOfFate.Controllers
{
    [ExceptionHandling]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EngineerController : ApiController
    {
        private readonly IEntryRepository _entryRepository;
        private readonly IEngineerRepository _engineerRepository;
        private readonly ISettingsProvider _settings;

        public EngineerController(IEngineerRepository engineerRepository, 
                                    ISettingsProvider settingsProvider, 
                                    IEntryRepository entryRepository)
        {
            _engineerRepository = engineerRepository;
            _settings = settingsProvider;
            _entryRepository = entryRepository;
        }

        /// <summary>
        /// Get all engineers
        /// </summary>
        /// <returns>Collection of engineers</returns>
        [HttpGet]
        public IEnumerable<Engineer> Get()
        {
            return _engineerRepository.GetAll();
        }

        /// <summary>
        /// Gets engineer with specified engineerId
        /// </summary>
        /// <param name="engineerId"></param>
        /// <returns></returns>
        [HttpGet]
        public Engineer Get(int engineerId)
        {
            var result = _engineerRepository.GetById(engineerId);
            if (result == null)
            {
                throw new BussinessException("Couldn't find engineer with id " + engineerId, HttpStatusCode.NotFound);
            }
            return result;
        }

        /// <summary>
        /// Updates an engineer or creates new engineer if engineer.EngineerId is not specified
        /// </summary>
        /// <param name="engineer"></param>
        [HttpPost]
        public void AddOrUpdate([FromBody]Engineer engineer)
        {
            if (engineer.EngineerId > 0)
            {
                _engineerRepository.UpdateEngineer(engineer);
            }
            else
            {
                var entries = _entryRepository.GetAll().ToList();
                var minDate = entries.Any() ? entries.Min(e => e.Date) : _settings.CurrentDate;
                var weekdaysCount = Helpers.WheelHelper.GetWeekdays(
                                Helpers.WheelHelper.GetPeriodDateRange(0, 
                                                                        minDate, 
                                                                        _settings.PeriodLength, 
                                                                        _settings.CurrentDate)).Count();
                if (weekdaysCount * _settings.ShiftsInADay <
                    _engineerRepository.GetAll().Count() * _settings.MinShiftsForEngineerPerPeriod)
                {
                    throw new BussinessException("adding an engineer would cause rule regarding minimum" +
                                                 " engineer’s shifts in period to never be fulfilled",
                                                 HttpStatusCode.BadRequest);
                }
                _engineerRepository.Create(engineer);
            }
            _engineerRepository.SaveChanges();
        }

        /// <summary>
        /// Deletes engineer with specified id
        /// </summary>
        [HttpDelete]
        public void Delete([FromBody]Engineer engineer)
        {
            if (engineer == null || engineer.EngineerId == 0)
            {
                throw new BussinessException("EngineerId must be provided!", HttpStatusCode.BadRequest);
            }
            _engineerRepository.DeleteById(engineer.EngineerId);
            _engineerRepository.SaveChanges();
        }
    }
}
