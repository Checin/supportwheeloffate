﻿using System.Collections.Generic;
using System.Web.Http;
using SupportWheelOfFate.Managers;
using SupportWheelOfFate.Models;
using System.Web.Http.Cors;
using SupportWheelOfFate.ErrorHandling;
using System;
using System.Linq;
using System.Net;

namespace SupportWheelOfFate.Controllers
{
    [ExceptionHandling]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EntryController : ApiController
    {
        private readonly WheelManager manager;

        public EntryController(WheelManager manager)
        {
            this.manager = manager;
        }

        /// <summary>
        /// Gets all entries
        /// </summary>
        /// <returns>List of Entry grouped by date</returns>
        [HttpGet]
        public List<IGrouping<DateTime, Entry>> Get()
        {
            return manager.GetEntriesForPeriod(0).GroupBy(e => e.Date).ToList();
        }

        /// <summary>
        /// Gets entries for specified period
        /// </summary>
        /// <returns>List of Entry grouped by date</returns>
        [HttpGet]
        public List<IGrouping<DateTime, Entry>> Get(int period)
        {
            return manager.GetEntriesForPeriod(period).GroupBy(e => e.Date).ToList();
        }

        /// <summary>
        /// Deletes all entries
        /// </summary>
        [HttpDelete]
        public void DeleteAll()
        {
            manager.DeleteAllEntries();
        }

        /// <summary>
        /// Deletes all entries in specified period
        /// </summary>
        /// <param name="period">Period number (0 for current period)</param>
        [HttpDelete]
        public void DeletePeriod([FromBody]PeriodNumber period)
        {
            if (period?.Period == null)
            {
                throw new BussinessException("Period number must be provided!", HttpStatusCode.BadRequest);
            }
            manager.DeleteEntriesForPeriod(period.Period.Value);
        }

        public class PeriodNumber
        {
            public int? Period { get; set; }
        }
    }
}
